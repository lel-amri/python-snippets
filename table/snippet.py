def format_table(rows, columns_formats=[]):
    for (align, type) in columns_formats:
        if align is not None and align not in ("<", ">", "=", "^"):
            raise ValueError("Invalid format: Invalid align")
        if type is not None and type not in ("b", "c", "d", "e", "E", "f", "F", "g", "G", "n", "o", "s", "x", "X", "%"):
            raise ValueError("Invalid format: Invalid type")
    columns_formats_ = []
    columns_widths = []
    for row in rows:
        for i, cell in enumerate(row):
            if i < len(columns_widths):
                columns_widths[i] = max(columns_widths[i], len(columns_formats_[i].format(cell)))
            elif i == len(columns_widths):
                try:
                    column_format = columns_formats[i]
                except IndexError:
                    align = ""
                    type = "s"
                else:
                    align, type = column_format
                fmt = "{{:{:s}{:s}}}".format("" if align is None else align, "s" if type is None else type)
                columns_formats_.append(fmt)
                columns_widths.append(len(fmt.format(cell)))
            elif i > len(columns_widths):
                assert False, "DEAD CODE"
    columns_formats_.clear()
    for width, (align, type) in zip(columns_widths, columns_formats):
        columns_formats_.append("{{:{:s}{:d}{:s}}}".format("" if align is None else align, width, "s" if type is None else type))
    for width in columns_widths[len(columns_formats_):]:
        columns_formats_.append("{{:{:d}s}}".format(width))
    fmt = "  ".join(columns_formats_) + "\n"
    return "".join(fmt.format(*(row + [""] * (len(columns_widths) - len(row)))) for row in rows)
