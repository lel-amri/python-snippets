def human_bytes_to_int(s):
    for i, c in enumerate(s):
        if not c.isdigit() or c in ("_", " ", "."):
            break
    else:
        return int(s)
    numeric_part = s[:i]
    unit_part = s[i:]
    numeric_part = numeric_part.replace("_", "").replace(" ", "")
    unit_part = unit_part.strip()
    if sum(1 if c == "." else 0 for c in numeric_part) > 1:
        raise ValueError("Malformed number")
    if "." in numeric_part:
        l, r = numeric_part.split(".", 1)
        l = l.lstrip("0")
        if len(l) == 0:
            l = "0"
        r = r.rstrip("0")
        if len(r) == 0:
            r = "0"
        amount = float(l) + float(r) / 10 ** len(r)
    else:
        amount = int(numeric_part)
    multipliers = {
        "B": 1,
        "kB": 1000**1,
        "MB": 1000**2,
        "GB": 1000**3,
        "TB": 1000**4,
        "PB": 1000**5,
        "EB": 1000**6,
        "ZB": 1000**7,
        "KiB": 1024**1,
        "MiB": 1024**2,
        "GiB": 1024**3,
        "TiB": 1024**4,
        "PiB": 1024**5,
        "EiB": 1024**6,
        "ZiB": 1024**7,
    }
    try:
        return amount * multipliers[unit_part]
    except KeyError:
        raise ValueError("Malformed unit") from None


def prepare_human_bytes(n, units_system="binary"):
    units = {
        "decimal": ["B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB"],
        "binary": ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB"],
    }
    rank = 0
    if units_system == "decimal":
        multiplier = 1000
    elif units_system == "binary":
        multiplier = 1024
    else:
        raise ValueError("Invalid value for units_system")
    while round(n) >= 10000:
        n /= multiplier
        rank += 1
    if n < 10:
        l = int(n)
        r = round((n - l) * 100)
    elif n < 100:
        l = int(n)
        r = round((n - l) * 10)
    elif n < 10000:
        l = round(n)
        r = 0
    else:
        assert False, "DEAD CODE"
    return (l, r, units[units_system][rank])


def format_human_bytes(n, units_system="binary"):
    l, r, u = prepare_human_bytes(n, units_system=units_system)
    if r == 0:
        fmt = "{l:d}{u:s}"
    else:
        fmt = "{l:d}.{r:d}{u:s}"
    return fmt.format(l=l, r=r, u=u)
